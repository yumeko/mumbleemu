// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Command goMumbleEmuCLI is an example/test for the go binding of libMumbleEmu
// It takes HTML files as arguments and renders them in a QTextEdit
package main

import (
	"fmt"
	"os"
	"path/filepath"

	"io/ioutil"
	"sync/atomic"

	"gitlab.com/yumeko/mumbleemu"
)

func init() {
	mumbleemu.Init()
}

var width = 400
var height = 400

// this synchronization scheme only works because MakeWindow blocks until Main is
// called, and then everything should be added by then.
var out = int32(0)

func ScrotFile(file string) {
	atomic.AddInt32(&out, 1)
	w := mumbleemu.NewWindow()
	defer func() {
		w.Destroy()
		if atomic.AddInt32(&out, -1) == 0 {
			mumbleemu.ExitMain()
		}
	}()
	w.Size(width, height)
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	w.HTML(bytes)
	base := filepath.Base(file)
	w.ScrotFile(base[:len(base)-len(filepath.Ext(base))]+".png", width, height)
}

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("goMumbleEmuCLI [files]...")
		return
	}
	for _, v := range os.Args[1:] {
		go ScrotFile(v)
	}
	mumbleemu.Main()
	fmt.Println("Main Exited")
}
