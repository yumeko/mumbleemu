#!/bin/bash
set -e -x
rm -rf build || true
mkdir build
pushd build
qmake -recursive ../MumbleEmu.pro CONFIG+="release"
make -j8
sudo make install
popd