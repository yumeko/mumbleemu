// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

#include "mumbleemu.h"
#include <cstring>
#include <thread>
#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
	MumbleEmu_Init(NULL,0);
	thread([&](){
		cout << "thread\n";
		MumbleEmu *win = MumbleEmu_Create();
		cout << "alloc\n";
		MumbleEmu_Size(win,400,800);
		if(argc==2){
			ifstream in(argv[1],ios::in|ios::binary);
			if(!in){
				MumbleEmu_SetValue(win,"File does not exist.",strlen("File does not exist."));
			}else{
				string str((istreambuf_iterator<char>(in)),istreambuf_iterator<char>());
				MumbleEmu_SetValue(win,str.data(),str.length());
			}
		}else{
			MumbleEmu_SetValue(win,"Hello<br>World",14);
		}
		MumbleEmu_ScrotFile(win,"emucli.png",400,400);
		cout << "wait\n";
		MumbleEmu_Destroy(win);
		MumbleEmu_Kill();
		cout << "kill\n";
	}).detach();
	cout << "running\n";
	MumbleEmu_Exec();
	cout << "exec done\n";
	return 0;
}
