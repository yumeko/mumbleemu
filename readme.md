MumbleEmu
=========
MumbleEmu provides a simple interface to QTextBrowser, the rendering engine used
in mumble. This is provided as a way to semi-automatically test things designed
to run in mumble's chat system.

This is designed for automated testing, not for end-user consumption.

There is a C interface, and a go interface ontop of that. The go interface works
out of the box on windows, however it uses many windows system calls to allow 
for the dll's not being in your $PATH