// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Package mumbleemu provides a interface to QTextBrowser, similar to what mumble
// uses for the chat log.
package mumbleemu

import (
	"image"
	"runtime"
	"unsafe"
)

// #cgo LDFLAGS: -lMumbleEmu
// #include "qt/mumbleemu.h"
import "C"

// Init must be called first thing in main.init
// This calls runtime.LockOSThread() and loads libMumbleEmu and Qt
// It will panic upon failure
func Init() {
	runtime.LockOSThread()
	C.MumbleEmu_Init(nil, 0)
}

// Main must be called in main.main(), It cannot be run in a separate goroutine.
// Additionally it may never return, but can be killed on most OSs by calling
// ExitMain.
func Main() {
	C.MumbleEmu_Exec()
}

// ExitMain cause Main to exit, and all other calls to block indefinatly
func ExitMain() {
	C.MumbleEmu_Kill()
}

// Window represents a QTextBrowser window
type Window [0]byte

// NewWindow creates a new QTextBrowser, empty and hidden
func NewWindow() *Window {
	win := C.MumbleEmu_Create()
	return (*Window)(unsafe.Pointer(win))
}
func (i *Window) Size(w, h int) {
	C.MumbleEmu_Size((*C.MumbleEmu)(unsafe.Pointer(i)), C.int(w), C.int(h))
}
func (i *Window) HTML(html []byte) {
	C.MumbleEmu_SetValue((*C.MumbleEmu)(unsafe.Pointer(i)), (*C.char)(unsafe.Pointer(&html[0])), C.size_t(len(html)))
}
func (i *Window) Stylesheet(style []byte) {
	C.MumbleEmu_SetStyleSheet((*C.MumbleEmu)(unsafe.Pointer(i)), (*C.char)(unsafe.Pointer(&style[0])), C.size_t(len(style)))
}
func (i *Window) Show() {
	C.MumbleEmu_Show((*C.MumbleEmu)(unsafe.Pointer(i)))
}

// Wait for the window to be closed(hidden).
func (i *Window) Wait() {
	C.MumbleEmu_Wait((*C.MumbleEmu)(unsafe.Pointer(i)))
}
func (i *Window) Hide() {
	C.MumbleEmu_Hide((*C.MumbleEmu)(unsafe.Pointer(i)))
}
func (i *Window) Scrot(w, h int) *image.NRGBA {
	b := make([]byte, w*h*4)
	C.MumbleEmu_Scrot((*C.MumbleEmu)(unsafe.Pointer(i)), (*C.char)(unsafe.Pointer(&b[0])), C.int(w), C.int(h), C.int(w*4))
	//Qt uses ARGB, go uses RGBA
	for i := 0; i < len(b); i += 4 {
		b[i+0], b[i+1], b[i+2], b[i+3] = b[i+1], b[i+2], b[i+3], b[i+0]
	}
	return &image.NRGBA{
		Pix:    b,
		Stride: w * 4,
		Rect:   image.Rect(0, 0, w, h),
	}
}
func (i *Window) ScrotFile(name string, w, h int) {
	b := append([]byte(name), 0)
	C.MumbleEmu_ScrotFile((*C.MumbleEmu)(unsafe.Pointer(i)), (*C.char)(unsafe.Pointer(&b[0])), C.int(w), C.int(h))
}
func (i *Window) Destroy() {
	C.MumbleEmu_Destroy((*C.MumbleEmu)(unsafe.Pointer(i)))
}
