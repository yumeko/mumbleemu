QT       += core widgets

TARGET = MumbleEmu
TEMPLATE = lib

QTPLUGIN += qjpeg +qgif

DEFINES += LIBMUMBLEEMU_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
  mumbleemu.cpp

HEADERS += \
  mumbleemu.h \
  mumbleemuclassdef.hpp

target.path = $$[QT_INSTALL_LIBS]
INSTALLS += target

headers.files = mumbleemu.h
headers.path = $$[QT_INSTALL_HEADERS]
INSTALLS += headers