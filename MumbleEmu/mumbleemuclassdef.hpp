// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

#ifndef MUMBLEEMUCLASSDEF_HPP
#define MUMBLEEMUCLASSDEF_HPP
#include <QDialog>
#include <QTextBrowser>

class MumbleEmu : public QDialog {
	Q_OBJECT
public:
	QTextBrowser tb;
	MumbleEmu();
public slots:
	void resize(int,int);
	void scrot(QImage*);
	bool isHidden();
	void setHTML(const QString &text);
signals:
	void onClose();
protected:
	void closeEvent(QCloseEvent *event);
};

class MumbleEmuProxy : public QObject {
	Q_OBJECT
public slots:
	MumbleEmu *Create();
};

#endif // MUMBLEEMUCLASSDEF_HPP
