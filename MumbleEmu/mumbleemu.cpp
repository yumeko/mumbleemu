// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

#include <QApplication>
#include <QPainter>
#include <QCloseEvent>

#include <QDebug>
#include <QImageReader>

#include "mumbleemu.h"
#include "mumbleemuclassdef.hpp"
MumbleEmu::MumbleEmu() : QDialog(), tb(this){
	setAttribute(Qt::WA_DeleteOnClose);
}
void MumbleEmu::closeEvent(QCloseEvent *ev){
	hide();
	ev->ignore();
}
void MumbleEmu::resize(int w, int h){
	this->tb.resize(w,h);
	QDialog::resize(w,h);
}
void MumbleEmu::scrot(QImage *im){
	QPainter p(im);
	p.setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
	this->tb.render(&p);
}
void MumbleEmu::setHTML(const QString &text){
	this->tb.setHtml(text);
}
bool MumbleEmu::isHidden(){
	return QDialog::isHidden();
}

MumbleEmu *MumbleEmuProxy::Create(){
	return new MumbleEmu();
}

QApplication *app=nullptr;
MumbleEmuProxy proxy;
extern "C" {
LMU_EXPORT void MumbleEmu_Init(const char* cRootPath, size_t len){
	if(app==nullptr){
		QList<QByteArray> argv;
		if(cRootPath!=NULL) {
			QString rootPath = QString::fromUtf8(cRootPath, len);
			argv << QString("%1/libMumbleEmu.dll").arg(rootPath).toLocal8Bit();
			argv << "-platformpluginpath";
			argv <<QString("%1/platforms").arg(rootPath).toLocal8Bit();
			QCoreApplication::addLibraryPath(QString("%1").arg(rootPath));
		} else {
			argv << "invalid";
		}
		int argvLen = 0;
		char **cargv = new char*[argv.length()];
		int *argc = new int(argv.length());
		for (auto s : argv) {
			argvLen += s.length() + 1;
		}
		char *cargvv = new char[argvLen];
		for (int i = 0; i < argv.length(); i++) {
			cargv[i] = cargvv;
			memcpy(cargvv, argv[i].constData(), argv[i].length());
			cargvv += argv[i].length();
			*cargvv++ = 0;
		}
		// must not free argc/v
		app = new QApplication(*argc, cargv);

		qDebug() << QCoreApplication::libraryPaths();
		qDebug() << QCoreApplication::applicationDirPath();
		qDebug() << QImageReader::supportedImageFormats();
		app->setQuitOnLastWindowClosed(false);
		proxy.moveToThread(app->thread());
	}
}
LMU_EXPORT void MumbleEmu_Exec(){
	if(app!=nullptr){
		app->exec();
	}
}

LMU_EXPORT void MumbleEmu_Kill(){
	if(app!=nullptr){
		app->exit(0);
	}
}

LMU_EXPORT MumbleEmu *MumbleEmu_Create(){
	MumbleEmu *mu;
	QMetaObject::invokeMethod(&proxy,"Create",Qt::BlockingQueuedConnection,Q_RETURN_ARG(MumbleEmu*,mu));
	return mu;
}

LMU_EXPORT void MumbleEmu_Size(MumbleEmu *e, int width, int height){
	QMetaObject::invokeMethod(e,"resize",Q_ARG(int,width),Q_ARG(int,height));
}
LMU_EXPORT void MumbleEmu_SetValue(MumbleEmu *e, const char *data, size_t len){
	QString str = QString::fromUtf8(data,len);
	QMetaObject::invokeMethod(e,"setHTML",Qt::BlockingQueuedConnection,Q_ARG(QString,str));
}
LMU_EXPORT void MumbleEmu_SetStyleSheet(MumbleEmu *e, const char* data, size_t len){
	QString str = QString::fromUtf8(data,len);
	QMetaObject::invokeMethod(e,"setStyleSheet",Q_ARG(QString,str));
}
LMU_EXPORT void MumbleEmu_Show(MumbleEmu *e){
	QMetaObject::invokeMethod(e,"show",Qt::BlockingQueuedConnection);
}
LMU_EXPORT void MumbleEmu_Wait(MumbleEmu *e){
	for(bool hidden=false;!hidden;){
		QMetaObject::invokeMethod(e,"isHidden",Qt::BlockingQueuedConnection, Q_RETURN_ARG(bool, hidden));
	}
}
LMU_EXPORT void MumbleEmu_Hide(MumbleEmu *e){
	QMetaObject::invokeMethod(e,"hide");
}
LMU_EXPORT void MumbleEmu_Scrot(MumbleEmu *e, char* data, int width, int height, int stride){
	QImage im((uchar*)data,width,height,stride,QImage::Format_ARGB32);
	QMetaObject::invokeMethod(e,"scrot",Qt::BlockingQueuedConnection,Q_ARG(QImage*,&im));
}
LMU_EXPORT void MumbleEmu_ScrotFile(MumbleEmu *e, const char *name, int width, int height){
	QImage im(width,height,QImage::Format_ARGB32);
	QMetaObject::invokeMethod(e,"scrot",Qt::BlockingQueuedConnection,Q_ARG(QImage*,&im));
	im.save(name);
}

LMU_EXPORT void MumbleEmu_Destroy(MumbleEmu *e){
	QMetaObject::invokeMethod(e,"close");
}

}
