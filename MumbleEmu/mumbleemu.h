// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

#ifndef MUMBLEEMU_H
#define MUMBLEEMU_H

#include "stddef.h"

#if defined(_WIN32) || defined(_WIN64)
	#ifdef LIBMUMBLEEMU_LIBRARY
		#define LMU_EXPORT __declspec(dllexport)
	#else
		#define LMU_EXPORT __declspec(dllimport)
	#endif
#else
	#define LMU_EXPORT
#endif

#ifdef LIBMUMBLEEMU_LIBRARY
class MumbleEmu;
#else
struct MumbleEmu;
typedef struct MumbleEmu MumbleEmu;
#endif

#ifdef __cplusplus
extern "C" {
#endif
// Run this first
LMU_EXPORT void MumbleEmu_Init(const char* libsPath, size_t len);
// Run this in the main thread. It runs until Kill is called
LMU_EXPORT void MumbleEmu_Exec();
// Stop Exec
LMU_EXPORT void MumbleEmu_Kill();

// Allocate and create a window
LMU_EXPORT MumbleEmu *MumbleEmu_Create();
// Set a window's size
LMU_EXPORT void MumbleEmu_Size(MumbleEmu*, int width, int height);
// Set the window's style sheet
LMU_EXPORT void MumbleEmu_SetStyleSheet(MumbleEmu*, const char* data, size_t len);
// Set the window's text
LMU_EXPORT void MumbleEmu_SetValue(MumbleEmu*, const char* data, size_t len);
// Show the window
LMU_EXPORT void MumbleEmu_Show(MumbleEmu*);
// Wait for the window to be hidden(closed)
LMU_EXPORT void MumbleEmu_Wait(MumbleEmu*);
// Hide the window
LMU_EXPORT void MumbleEmu_Hide(MumbleEmu*);
// Take a screenshot and place it in data[:len]
// Strides must be 32bit aligned
LMU_EXPORT void MumbleEmu_Scrot(MumbleEmu*, char* data, int width, int height, int stride);
// Scrot to a file
LMU_EXPORT void MumbleEmu_ScrotFile(MumbleEmu *e, const char *name, int width, int height);
// Free the window
LMU_EXPORT void MumbleEmu_Destroy(MumbleEmu*);

#ifdef __cplusplus
}
#endif

#endif // MUMBLEEMU_H
