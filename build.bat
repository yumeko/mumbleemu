REM this is for Visual studio QT only, if you have QT build for MinGW use that.
REM just qmake && make && go install, or something. I havn't tried yet
pushd .
REM Setup QT (change this)
call "C:\Qt\5.9\msvc2015_64\bin\qtenv2.bat"
REM Setup Visual Studio (this is 2015, may have to change)
if not defined DevEnvDir (
	call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
)
popd
mkdir build
pushd build
qmake -recursive ../MumbleEmu.pro CONFIG+="release" || popd && exit /b
nmake || popd && exit /b
popd
rm -rf winbin
mkdir winbin || exit /b
cp build/MumbleEmuCLI/release/MumbleEmuCLI.exe winbin || exit /b
cp build/libMumbleEmu/release/libMumbleEmu.dll winbin || exit /b
cp build/libMumbleEmu/release/libMumbleEmu.lib winbin || exit /b
windeployqt winbin/libMumbleEmu.dll --no-translations --no-compiler-runtime