// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Package mumbleemu provides a interface to QTextBrowser, similar to what mumble
// uses for the chat log.
package mumbleemu

import (
	"fmt"
	"image"
	"reflect"
	"runtime"
	"syscall"
	"unsafe"

	"bitbucket.org/abex/pathfinder"

	"golang.org/x/sys/windows"
)

// Init must be called first thing in main.init
// This calls runtime.LockOSThread() and loads libMumbleEmu and Qt
// It will panic upon failure
func Init() {
	runtime.LockOSThread()
	path, err := pathfinder.FindDir("gitlab.com/yumeko/mumbleemu", "winbin")
	if err != nil {
		panic(err)
	}
	k32 := windows.MustLoadDLL("kernel32.dll")
	defer k32.Release()
	k32.MustFindProc("SetDefaultDllDirectories").Call(0x1000)
	k32.MustFindProc("AddDllDirectory").Call(uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(path))))
	lmu := windows.MustLoadDLL("libMumbleEmu.dll")
	bpath := []byte(path)
	lmu.MustFindProc("MumbleEmu_Init").Call(uintptr(unsafe.Pointer(&bpath[0])), uintptr(len(bpath)))
	vfns := reflect.ValueOf(&fns).Elem()
	tfns := vfns.Type()
	for i := 0; i < tfns.NumField(); i++ {
		name := fmt.Sprintf("MumbleEmu_%v", tfns.Field(i).Name)
		vfns.Field(i).Set(reflect.ValueOf(lmu.MustFindProc(name)))
	}
}

var fns = struct {
	Exec          *windows.Proc
	Kill          *windows.Proc
	Create        *windows.Proc
	Size          *windows.Proc
	SetValue      *windows.Proc
	SetStyleSheet *windows.Proc
	Show          *windows.Proc
	Wait          *windows.Proc
	Hide          *windows.Proc
	Scrot         *windows.Proc
	ScrotFile     *windows.Proc
	Destroy       *windows.Proc
}{}

// Main must be called in main.main(), It cannot be run in a separate goroutine.
// Additionally it may never return, but can be killed on most OSs by calling
// ExitMain.
func Main() {
	fns.Exec.Call()
}

// ExitMain cause Main to exit, and all other calls to block indefinatly
func ExitMain() {
	fns.Kill.Call()
}

// Window represents a QTextBrowser window
type Window [0]byte

// NewWindow creates a new QTextBrowser, empty and hidden
func NewWindow() *Window {
	win, _, _ := fns.Create.Call()
	return (*Window)(unsafe.Pointer(win))
}
func (i *Window) Size(w, h int) {
	fns.Size.Call(uintptr(unsafe.Pointer(i)), uintptr(w), uintptr(h))
}
func (i *Window) HTML(html []byte) {
	fns.SetValue.Call(uintptr(unsafe.Pointer(i)), uintptr(unsafe.Pointer(&html[0])), uintptr(len(html)))
}
func (i *Window) Stylesheet(style []byte) {
	fns.SetStyleSheet.Call(uintptr(unsafe.Pointer(i)), uintptr(unsafe.Pointer(&style[0])), uintptr(len(style)))
}
func (i *Window) Show() {
	fns.Show.Call(uintptr(unsafe.Pointer(i)))
}

// Wait for the window to be closed(hidden).
func (i *Window) Wait() {
	fns.Wait.Call(uintptr(unsafe.Pointer(i)))
}
func (i *Window) Hide() {
	fns.Hide.Call(uintptr(unsafe.Pointer(i)))
}
func (i *Window) Scrot(w, h int) *image.NRGBA {
	b := make([]byte, w*h*4)
	fns.Scrot.Call(uintptr(unsafe.Pointer(i)), uintptr(unsafe.Pointer(&b[0])), uintptr(w), uintptr(h), uintptr(w*4))
	//Qt uses ARGB, go uses RGBA
	for i := 0; i < len(b); i += 4 {
		b[i+0], b[i+1], b[i+2], b[i+3] = b[i+1], b[i+2], b[i+3], b[i+0]
	}
	return &image.NRGBA{
		Pix:    b,
		Stride: w * 4,
		Rect:   image.Rect(0, 0, w, h),
	}
}
func (i *Window) ScrotFile(name string, w, h int) {
	b := append([]byte(name), 0)
	fns.ScrotFile.Call(uintptr(unsafe.Pointer(i)), uintptr(unsafe.Pointer(&b[0])), uintptr(w), uintptr(h))
}
func (i *Window) Destroy() {
	fns.Destroy.Call(uintptr(unsafe.Pointer(i)))
}
