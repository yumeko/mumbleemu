// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Package tabletester provides simple test tables for mumble chat
package tabletester

//go:generate go run styles_gen.go

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sync"

	"os"

	"gitlab.com/yumeko/mumbleemu"
)

func Init() {
	mumbleemu.Init()
}

type work struct {
	name  string
	value []byte
}
type Suite struct {
	done      sync.WaitGroup
	workers   []chan *work
	outputdir string
	w, h      int
	tests     []string
}

func NewSuite(outputdir string, width int, height int) *Suite {
	s := &Suite{
		outputdir: outputdir,
		w:         width,
		h:         height,
	}
	err := os.RemoveAll(outputdir)
	if err != nil {
		panic(err)
	}
	err = os.MkdirAll(outputdir, 0775)
	if err != nil {
		panic(err)
	}
	return s
}

func (s *Suite) AddStyle(name, contents string) {
	c := make(chan *work)
	s.done.Add(1)
	s.workers = append(s.workers, c)
	go func() {
		defer s.done.Done()
		win := mumbleemu.NewWindow()
		defer win.Destroy()
		if contents != "" {
			win.Stylesheet([]byte(contents))
		}
		win.Size(s.w, s.h)
		win.Show()
		for w := range c {
			finame := filepath.Join(s.outputdir, fmt.Sprintf("%v_%v.png", w.name, name))
			err := os.MkdirAll(filepath.Dir(finame), 0775)
			if err != nil {
				panic(err)
			}
			win.HTML(w.value)
			//time.Sleep(time.Second)
			win.ScrotFile(finame, s.w, s.h)
		}
	}()
}
func (s *Suite) TestFile(name string) {
	b, err := ioutil.ReadFile(name)
	if err != nil {
		panic(err)
	}
	name = filepath.Base(name)
	name = name[:len(name)-len(filepath.Ext(name))]
	s.Test(name, b)
}
func (s *Suite) Test(name string, source []byte) {
	s.tests = append(s.tests, name)
	for _, v := range s.workers {
		v <- &work{
			name,
			source,
		}
	}
}
func (s *Suite) Finish() {
	for _, v := range s.workers {
		close(v)
	}
	s.done.Wait()
}

func AllDone() {
	mumbleemu.ExitMain()
}
func Main() {
	mumbleemu.Main()
}
