// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// +build none

package main

import (
	"fmt"
	"strings"

	tabletester "gitlab.com/yumeko/mumbleemu/tabletester"
)

func init() {
	tabletester.Init()
}

func main() {
	go func() {
		test := tabletester.NewSuite("example", 300, 100)
		test.AddDefaultStyles()
		for _, line := range strings.Split(table, "\n") {
			n := strings.SplitN(line, "\t", 3)
			test.Test(n[0], []byte(fmt.Sprintf("%v<%v>value</%v>%v", n[0], n[0], n[1], n[2])))
		}
		test.Finish()
		tabletester.AllDone()
	}()
	tabletester.Main()
}

var table = `a	Anchor or link	Supports the href and name attributes.
address	Address	
b	Bold	
big	Larger font	
blockquote	Indented paragraph	
body	Document body	Supports the bgcolor attribute, which can be a Qt color name or a #RRGGBB color specification.
br	Line break	
center	Centered paragraph	
cite	Inline citation	Same as i.
code	Code	Same as tt.
dd	Definition data	
dfn	Definition	Same as i.
div	Document division	Supports the standard block attributes.
dl	Definition list	Supports the standard block attributes.
dt	Definition term	Supports the standard block attributes.
em	Emphasized	Same as i.
font	Font size, family, and/or color	Supports the following attributes: size, face, and color (Qt color names or #RRGGBB).
h1	Level 1 heading	Supports the standard block attributes.
h2	Level 2 heading	Supports the standard block attributes.
h3	Level 3 heading	Supports the standard block attributes.
h4	Level 4 heading	Supports the standard block attributes.
h5	Level 5 heading	Supports the standard block attributes.
h6	Level 6 heading	Supports the standard block attributes.
head	Document header	
hr	Horizontal line	Supports the width attribute, which can be specified as an absolute or relative (%) value.
html	HTML document	
i	Italic	
img	Image	Supports the src, source (for Qt 3 compatibility), width, and height attributes.
kbd	User-entered text	
li	List item	
nobr	Non-breakable text	
ol	Ordered list	Supports the standard list attributes.
p	Paragraph	Left-aligned by default. Supports the standard block attributes.
pre	Preformated text	
qt	Qt rich-text document	Synonym for html. Provided for compatibility with earlier versions of Qt.
s	Strikethrough	
samp	Sample code	Same as tt.
small	Small font	
span	Grouped elements	
strong	Strong	Same as b.
sub	Subscript	
sup	Superscript	
table	Table	Supports the following attributes: border, bgcolor (Qt color names or #RRGGBB), cellspacing, cellpadding, width (absolute or relative), and height.
tbody	Table body	Does nothing.
td	Table data cell	Supports the standard table cell attributes.
tfoot	Table footer	Does nothing.
th	Table header cell	Supports the standard table cell attributes.
thead	Table header	If the thead tag is specified, it is used when printing tables that span multiple pages.
title	Document title	The value specified using the title tag is available through QTextDocument::metaInformation().
tr	Table row	Supports the bgcolor attribute, which can be a Qt color name or a #RRGGBB color specification.
tt	Typewrite font	
u	Underlined	
ul	Unordered list	Supports the standard list attributes.
var	Variable	Same as i.`
