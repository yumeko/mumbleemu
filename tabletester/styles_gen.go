// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

//+build none

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

var styles = []string{
	"Dark",
	"Lite",
}

func main() {
	fi, err := os.OpenFile("styles.gen.go", os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0777)
	if err != nil {
		panic(err)
	}
	defer fi.Close()

	fmt.Fprint(fi, `package tabletester

`)
	fmt.Fprint(fi, `func (s *Suite) AddDefaultStyles() {
	s.AddStyle("none", "")
`)
	for _, name := range styles {
		c, err := ioutil.ReadFile("../mumble-theme/" + name + ".qss")
		if err != nil {
			panic(err)
		}

		fmt.Fprintf(fi, "	s.AddStyle(%q, %q)\n", name, string(c))
	}
	fmt.Fprint(fi, `}
`)
}
